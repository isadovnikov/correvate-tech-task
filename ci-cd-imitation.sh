#test
mvn clean verify
#build
mvn clean package
docker build -f src/main/docker/Dockerfile -t  correvate/file-service .
#Deploy
#Port can be changed by server port configuration in docker file
docker run -i --rm -p 8080:8080 correvate/file-service
