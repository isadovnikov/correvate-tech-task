package com.isadounikau.correvatetechtask

import org.springframework.beans.factory.annotation.Autowired

class LoadContextIntegrationSpec extends AbstractIntegrationSpec {

    @Autowired
    public FilesController filesController

    def "when context is loaded then all expected beans are created"() {
        expect: "the Controllers are created"
        filesController
    }
}
