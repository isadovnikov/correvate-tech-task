package com.isadounikau.correvatetechtask

import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder
import org.springframework.util.ResourceUtils
import spock.lang.Shared
import spock.lang.Unroll

import java.util.concurrent.Callable
import java.util.concurrent.Executors

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class FilesControllerIntegrationSpec extends AbstractIntegrationSpec {

    @Shared
    private def TEST_FILES_MAP = [
            "NORMAL_ONE"  : [
                    new MockMultipartFile("file", "file", "image/png", "test".getBytes())
            ],
            "NORMAL_TWO"  : [
                    new MockMultipartFile("file", "file", "image/png", "test".getBytes()),
                    new MockMultipartFile("file", "file", "image/png", "test".getBytes()),
            ],
            "TOO_BIG_FILE": [
                    new MockMultipartFile("file", "file", "image/png", ResourceUtils.getFile("classpath:files/lenna.png").bytes)
            ]
    ]

    @Unroll
    def 'when zipping #type files then #expectedCode is expected'() {
        given: 'a mockMultipartFile'
        files

        when: 'calling the post endpoint to create a new document'

        def multipart = multipart('/api/v1/files/compress')
        files.forEach {
            multipart.file(it)
        }

        def resultActions = mvc.perform(multipart)

        then: 'the status is #expectedCode'
        resultActions.andExpect(status().is(expectedCode.value()))

        where:
        type           | files                || expectedCode
        "NORMAL_ONE"   | TEST_FILES_MAP[type] || HttpStatus.CREATED
        "NORMAL_TWO"   | TEST_FILES_MAP[type] || HttpStatus.CREATED
        "TOO_BIG_FILE" | TEST_FILES_MAP[type] || HttpStatus.CREATED // DON'T KNOW EASY WAY TO TEST IT WITH MOCK MVC
    }

    @Unroll
    def 'when two api calls then 429 is expected'() {
        given: 'a mockMultipartFile'
        def multipart = multipart('/api/v1/files/compress')
        multipart.file(new MockMultipartFile("file", "file", "image/png", ResourceUtils.getFile("classpath:files/lenna.png").bytes))

        def executor = Executors.newCachedThreadPool()

        def firstApiCall = new ApiCall(multipart)
        def secondApiCall = new ApiCall(multipart)

        when: 'calling the post endpoint to create a new document'
        def results = executor.invokeAll(List.of(firstApiCall, secondApiCall))

        then:
        "DON'T KNOW EASY WAY TO TEST IT WITH MOCK MVC"
    }

    private class ApiCall implements Callable<ResultActions> {

        private MockMultipartHttpServletRequestBuilder multipart

        ApiCall(MockMultipartHttpServletRequestBuilder multipart) {
            this.multipart = multipart
        }

        @Override
        ResultActions call() throws Exception {
            return mvc.perform(multipart)
        }
    }
}
