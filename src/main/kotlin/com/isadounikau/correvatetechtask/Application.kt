package com.isadounikau.correvatetechtask

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import java.util.concurrent.RejectedExecutionHandler
import java.util.concurrent.ThreadPoolExecutor
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@EnableConfigurationProperties(ThreadPoolTaskExecutorProperties::class)
@SpringBootApplication
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}

@RestController
@RequestMapping("/api/v1/files")
class FilesController {

    @PostMapping(
        "/compress",
        consumes = [MediaType.MULTIPART_FORM_DATA_VALUE],
        produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE]
    )
    @ResponseStatus(HttpStatus.CREATED)
    fun createTransaction(
        request: MultipartHttpServletRequest
    ): StreamingResponseBody = StreamingResponseBody { out ->
        val zos = ZipOutputStream(out)
        zos.use {
            request.multiFileMap.flatMap { (prefix, files) ->
                files.map { file -> Pair(prefix.trim(), file) }
            }.map { (prefix, file) ->
                val fileName = generateZipEntryFilename(prefix, file.originalFilename)
                val ze = ZipEntry(fileName)
                zos.putNextEntry(ze)
                file.inputStream.use { inputStream ->
                    inputStream.transferTo(zos)
                }
                zos.closeEntry()
            }
        }
    }

    private fun generateZipEntryFilename(prefix: String, originalFilename: String?): String =
        "${prefix}_${System.currentTimeMillis()}_${originalFilename}"
}

@ConfigurationProperties(prefix = "application.thread-pool")
class ThreadPoolTaskExecutorProperties {
    var maxSize: Int = 10
    var maxQueueCapacity: Int = 10
}

@Configuration
class WebMvcConfigurer(
    private val properties: ThreadPoolTaskExecutorProperties,
    private val handler: BackPressureRejectedExecutionHandler,
) : WebMvcConfigurer {

    override fun configureAsyncSupport(configurer: AsyncSupportConfigurer) {
        ThreadPoolTaskExecutor().apply {
            maxPoolSize = properties.maxSize
            setQueueCapacity(properties.maxQueueCapacity)
            setRejectedExecutionHandler(handler)
        }.also {
            it.initialize()
        }.also {
            configurer.setTaskExecutor(it)
        }
    }
}

@Component
class BackPressureRejectedExecutionHandler : RejectedExecutionHandler {
    override fun rejectedExecution(r: Runnable, executor: ThreadPoolExecutor) {
        throw TooManyRequestsException("You exited the request limit")
    }
}

//More edge cases can be handled by @ControllerAdvice
@ResponseStatus(code = HttpStatus.TOO_MANY_REQUESTS, reason = "You exited the request limit")
class TooManyRequestsException(message: String) : RuntimeException(message)
