## SDLC

### Business Requirements
All requirements have been collected from
[file](Lead developer test.pdf)
Unfortunately, I didn't have the opportunity to ask questions

### Design
What is the high vision of the application?

**Main architecture points**
- Must handle XXX api requests per second
- Resource Usage XXX cpu XXX ram
- Must be cloud based
- Should be scalable
- Should be serverless
- Main Required Framework is Spring Boot
- Should be configurable

**Code quality**
- The business logic must be reliable
- The code must have good automated test coverage, minimum XX%
- Service APIs has to provide clear documentation

### Development
**Software Problems & Solutions**

1. Huge binary files
   - Use streams, avoid write bytes to hard memory
2. Executable files
   - Use streams, avoid write bytes to hard memory
3. Too many requests
   - Use backpressure
4. The same names for different files in request
   - Use timestamp, it is silly solution but works
5. The original file filename
   - Put at the end of the new filename, it resolves the problem plus save extension
6. Authentication?
   - It wasn't in the requirements, but for such kind of system the token based credit system will be a good option
7. Deployment
   - Pure docker file, can be used with in CD pipeline
   - Docker file with application can be easily deployed to any cloud platform
9. Metrics/Alerts
   - Actuator can be used + Prometheus + Micrometer
    
### Testing
**Automation**
- The all business edge cases should be covered by integration tests
- All complex logic must be covered by unit tests 

**Manual**

- Postman can be used, however, it has own limitations, huge files for example
- OpenAPI is a good option, however, it doesn't have decent support for form-data
- cURL is reliable as russian AK-47. Always works  

### Integration
This step wasn't required by the technical task. 
The deployment of the new application version can be done by almost any integration tool at the market 

The application can be run locally by the [script](ci-cd-imitation.sh). Command in project root `./ci-cd-imitation.sh`


### Support
This step wasn't required by the technical task.
Application logs and metrics are the best fiends for the next application iterations 

### Note
I have mixed feelings about all code in one file. I never tried it, so I decided to give it a try

Alternatives:
- Hexagonal architecture: too complex for simple tech task
- Package by a feature: nice one, recommend for microservices
- MVC: classical one. This one is the most appropriate here
